package silof

import (
	"fmt"
	"os"
	"path"
	"runtime"
	"time"
)

// Logger is used to perform logging operations.
type Logger struct {
	Name  string
	Sinks map[Sink]LogLevel
}

// NewLogger creates a new logger.
func NewLogger(name string) *Logger {
	return &Logger{
		Name:  name,
		Sinks: make(map[Sink]LogLevel, 10),
	}
}

// Close closes all the sinks associated with the logger.
func (logger *Logger) Close() {
	for sink := range logger.Sinks {
		sink.Close()
	}
}

// AddSink adds a sink to the logger.
func (logger *Logger) AddSink(minLevel LogLevel, sink Sink) {
	logger.Sinks[sink] = minLevel
}

// log writes the arguments with the specified log level to the sinks added to
// the logger.
func (logger *Logger) log(wrapperDepth int, level LogLevel, args ...interface{}) error {
	// Convert args appropriately.
	var data interface{}
	if len(args) == 0 {
		data = nil
	} else if len(args) == 1 {
		data = args[0]
	} else {
		data = args
	}
	// Create the log object.
	log := Log{
		Data:  data,
		Level: level,
		Name:  logger.Name,
		PID:   os.Getpid(),
		Time:  time.Now(),
	}
	// Get the hostname.
	if hostname, err := os.Hostname(); err != nil {
		log.Hostname = hostname
	}
	// Get the location.
	if _, file, line, ok := runtime.Caller(wrapperDepth); ok {
		log.Location = fmt.Sprintf("%s:%d", path.Base(file), line)
	}
	// Send out the log.
	for sink, minLevel := range logger.Sinks {
		if level >= minLevel {
			sink.Log(log) // TODO Handle errors?
		}
	}
	// Fatal means fatal.
	if level >= FATAL {
		fmt.Sprint(data)
		os.Exit(-1)
	}
	return nil
}

// Trace logs with log level trace.
func (logger *Logger) Trace(args ...interface{}) error {
	return logger.log(2, TRACE, args...)
}

// Debug logs with log level debug.
func (logger *Logger) Debug(args ...interface{}) error {
	return logger.log(2, DEBUG, args...)
}

// Info logs with log level info.
func (logger *Logger) Info(args ...interface{}) error {
	return logger.log(2, INFO, args...)
}

// Warn logs with log level warn.
func (logger *Logger) Warn(args ...interface{}) error {
	return logger.log(2, WARN, args...)
}

// Error logs with log level error.
func (logger *Logger) Error(args ...interface{}) error {
	return logger.log(2, ERROR, args...)
}

// Fatal logs with log level fatal.
func (logger *Logger) Fatal(args ...interface{}) error {
	return logger.log(2, FATAL, args...)
}
