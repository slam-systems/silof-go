package silof

type Sink interface {
	Close() error
	Log(Log) error
}
