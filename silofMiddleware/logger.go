package silofMiddleware

import (
	"net/http"
	"time"

	"github.com/codegangsta/negroni"
	"slam-systems.com/silof"
)

type LoggerMiddleware struct {
	Logger *silof.Logger
}

func NewLoggerMiddleware(logger *silof.Logger) *LoggerMiddleware {
	return &LoggerMiddleware{
		Logger: logger,
	}
}
func (m *LoggerMiddleware) ServeHTTP(w http.ResponseWriter, req *http.Request, next http.HandlerFunc) {
	m.Logger.Info("Starting", req.Method, req.URL)
	start := time.Now()
	next(w, req)
	res := w.(negroni.ResponseWriter)
	m.Logger.Info("Finished", res.Status(), http.StatusText(res.Status()), time.Since(start))
}
