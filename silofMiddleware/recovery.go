package silofMiddleware

import (
	"net/http"
	"runtime"

	"slam-systems.com/silof"
)

type Recoverer interface {
	ServeHTTP(w http.ResponseWriter, r *http.Request, err interface{})
}

type RecoveryMiddleware struct {
	Recoverer Recoverer
	Logger    *silof.Logger
}

func NewRecoveryMiddleware(logger *silof.Logger, recoverer Recoverer) *RecoveryMiddleware {
	return &RecoveryMiddleware{
		Recoverer: recoverer,
		Logger:    logger,
	}
}
func (m *RecoveryMiddleware) ServeHTTP(w http.ResponseWriter, req *http.Request, next http.HandlerFunc) {
	defer func() {
		if err := recover(); err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			b := make([]byte, 16384)
			runtime.Stack(b, false)
			m.Logger.Error(err, string(b))
			if m.Recoverer != nil {
				m.Recoverer.ServeHTTP(w, req, err)
			} else {
				w.Write(b)
			}
		}
	}()
	next(w, req)
}
