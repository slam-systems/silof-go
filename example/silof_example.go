package main

import silof ".."

func main() {
	logger := silof.NewLogger("silof_example")
	defer logger.Close()
	logger.AddSink(silof.LogLevelFromString("trace"), silof.NewConsoleSink())
	fileSink, err := silof.NewFileSink("example.log")
	if err != nil {
		panic(err)
	}
	logger.AddSink(silof.WARN, fileSink)

	logger.Trace("trace", silof.TRACE)
	logger.Debug("debug", silof.DEBUG)
	logger.Info("info", silof.INFO)
	logger.Warn("warn", silof.WARN)
	logger.Error("error", silof.ERROR)
	logger.Fatal("fatal", silof.FATAL)
}
