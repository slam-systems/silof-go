package silof

import (
	"encoding/json"
	"fmt"
	"os"
)

type FileSink struct {
	File *os.File
}

func NewFileSink(filename string) (*FileSink, error) {
	file, err := os.OpenFile(filename, os.O_WRONLY|os.O_APPEND|os.O_CREATE, 0644)
	if err == nil {
		return &FileSink{File: file}, nil
	} else {
		return nil, err
	}
}
func (sink *FileSink) Close() error {
	return sink.File.Close()
}
func (sink *FileSink) Log(log Log) error {
	// Convert the log to JSON
	b, err := json.Marshal(log)
	if err != nil {
		return err
	}
	// Write the JSON
	_, err = fmt.Fprintln(sink.File, string(b))
	return err
}
