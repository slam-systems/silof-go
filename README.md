# SiLoF #

**Si**mple **Lo**gging **F**ramework

This is similar to [Bunyan](https://github.com/trentm/node-bunyan), but for Go.
The goal is to make a logging format that's easy to aggregate. The solution
should scale from a single machine to a cluster of hundreds, and be based on
open technologies.
