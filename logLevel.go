package silof

import (
	"strconv"
	"strings"
)

type LogLevel uint

const (
	ALL LogLevel = iota * 10
	TRACE
	DEBUG
	INFO
	WARN
	ERROR
	FATAL
)

func LogLevelFromString(str string) LogLevel {
	switch strings.ToLower(str) {
	case "all":
		return ALL
	case "trace":
		return TRACE
	case "debug":
		return DEBUG
	case "info":
		return INFO
	case "warn":
		return WARN
	case "error":
		return ERROR
	case "fatal":
		return FATAL
	}
	if i, err := strconv.Atoi(str); err == nil {
		return LogLevel(i)
	} else {
		return INFO
	}
}
func (l LogLevel) String() string {
	switch l {
	case ALL:
		return "ALL"
	case TRACE:
		return "TRACE"
	case DEBUG:
		return "DEBUG"
	case INFO:
		return "INFO"
	case WARN:
		return "WARN"
	case ERROR:
		return "ERROR"
	case FATAL:
		return "FATAL"
	default:
		return strconv.Itoa(int(l))
	}
}
