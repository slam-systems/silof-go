package silof

import (
	"bytes"
	"fmt"
	"os"

	"github.com/fatih/color"
)

const ConsoleSinkTimeFormat = "2006-01-02 15:04:05.999Z0700"

func (l LogLevel) Color() *color.Color {
	n := uint(l)
	if n < uint(INFO) {
		return color.New(color.FgMagenta)
	} else if n < uint(WARN) {
		return color.New(color.FgCyan)
	} else if n < uint(ERROR) {
		return color.New(color.FgYellow)
	} else if n < uint(FATAL) {
		return color.New(color.FgRed)
	} else {
		return color.New(color.FgRed, color.Bold, color.Underline)
	}
}

type ConsoleSink struct{}

func NewConsoleSink() *ConsoleSink     { return &ConsoleSink{} }
func (sink *ConsoleSink) Close() error { return nil }
func (sink *ConsoleSink) Log(log Log) error {
	var hostname, location string
	bold := color.New(color.Bold).SprintFunc()
	if log.Location != "" {
		location = fmt.Sprintf("(%s)", log.Location)
	}
	if log.Hostname != "" {
		hostname = "@" + log.Hostname
	}
	tocolor := log.Level.Color().SprintFunc()
	_, err := fmt.Fprintf(os.Stderr, "[%s] %s%s%s/%d: %s %s\n",
		color.CyanString(log.Time.UTC().Format(ConsoleSinkTimeFormat)),
		bold(log.Name), location, hostname, log.PID,
		tocolor(log.Level), tocolor(sink.format(log.Data)))
	return err
}

func (sink *ConsoleSink) format(i interface{}) string {
	var b bytes.Buffer
	var f = true
	if _, ok := i.([]interface{}); !ok {
		i = []interface{}{i}
	}
	for _, d := range i.([]interface{}) {
		if f {
			f = false
		} else {
			b.WriteRune(' ')
		}
		b.WriteString(fmt.Sprint(d))
	}
	return b.String()
}
