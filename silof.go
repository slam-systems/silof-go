package silof // import "slam-systems.com/silof"

import "time"

type Log struct {
	Data     interface{} `json:"data"`
	Hostname string      `json:"hostname"`
	Level    LogLevel    `json:"level"`
	Location string      `json:"location"`
	Name     string      `json:"name"`
	PID      int         `json:"pid"`
	Time     time.Time   `json:"time"`
}
